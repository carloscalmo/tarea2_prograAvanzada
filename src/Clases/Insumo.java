package Clases;

/**
 * Created by carlos on 07/07/2017.
 */
public class Insumo {
    String descripcion;
    String unidad;
    Float cantidad;
    Float precioUnitario;
    Float precioTotal;

    public Insumo() {
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public Float getCantidad() {
        return cantidad;
    }

    public void setCantidad(Float cantidad) {
        this.cantidad = cantidad;
    }

    public Float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(Float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Float calculatePrecioTotal() {
        this.precioTotal = cantidad*precioUnitario;
        return this.precioTotal;
    }
}
