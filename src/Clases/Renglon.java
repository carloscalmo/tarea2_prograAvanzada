package Clases;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by carlos on 07/07/2017.
 */
public class Renglon {
    private String nombre;
    private String unidad;
    private float cantidad;
    private float precioUnitario;
    private float precioGlobal;
    private List<Insumo> listadoInsumos;

    public Renglon(){
        //your code here
        listadoInsumos = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public String getUnidad() {
        return unidad;

    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public float getPrecioGlobal() {
        return precioGlobal;
    }

    //calcular precio global
    public float setPrecioGlobal(){
        float global = 0;
        global = cantidad*precioUnitario;
        precioGlobal = global;
        return precioGlobal;
    }


    public void adicionarInsumos(Insumo insumo){
        listadoInsumos.add(insumo);
    }



}
