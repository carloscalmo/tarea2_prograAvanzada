package GUI;

import javax.swing.table.DefaultTableModel;

public class Insumo extends javax.swing.JInternalFrame {

    public Insumo() {
        initComponents();
        initTableInsumo();
    }
    
    public DefaultTableModel modeloInsumo = new DefaultTableModel();  
    
    private void initTableInsumo(){
        modeloInsumo.addColumn("Nombre");
        modeloInsumo.addColumn("Unidad");
        modeloInsumo.addColumn("Cantidad");
        modeloInsumo.addColumn("Precio Unitario");
        modeloInsumo.addColumn("Precio Total");        
        tableInsumo.setModel(modeloInsumo);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        fieldNombreInsumo = new javax.swing.JTextField();
        fieldUnidad = new javax.swing.JTextField();
        fieldCantidad = new javax.swing.JTextField();
        fieldPrecioUnitario = new javax.swing.JTextField();
        fieldPrecioGlobal = new javax.swing.JTextField();
        botonGuardarInsumo = new javax.swing.JButton();
        botonEliminarInsumo = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableInsumo = new javax.swing.JTable();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Insumo"));
        setClosable(true);
        setMaximizable(true);

        jLabel1.setText("Nombre:");

        jLabel2.setText("Unidad:");

        jLabel5.setText("Cantidad:");

        jLabel3.setText("Precio Unitario:");

        jLabel4.setText("Precio Global:");

        fieldPrecioGlobal.setEditable(false);
        fieldPrecioGlobal.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                fieldPrecioGlobalFocusGained(evt);
            }
        });
        fieldPrecioGlobal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fieldPrecioGlobalActionPerformed(evt);
            }
        });

        botonGuardarInsumo.setText("Guardar");
        botonGuardarInsumo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarInsumoActionPerformed(evt);
            }
        });

        botonEliminarInsumo.setText("Eliminar");
        botonEliminarInsumo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarInsumoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(fieldNombreInsumo, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(fieldPrecioGlobal, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(fieldPrecioUnitario, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(fieldCantidad, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(fieldUnidad, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 349, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botonGuardarInsumo)
                            .addComponent(botonEliminarInsumo))
                        .addGap(92, 92, 92))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldNombreInsumo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(fieldUnidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(fieldCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(fieldPrecioUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(botonGuardarInsumo)
                        .addGap(42, 42, 42)
                        .addComponent(botonEliminarInsumo)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(fieldPrecioGlobal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(74, Short.MAX_VALUE))
        );

        jScrollPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jScrollPane1MouseClicked(evt);
            }
        });

        tableInsumo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tableInsumo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableInsumoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableInsumo);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fieldPrecioGlobalFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fieldPrecioGlobalFocusGained
        Float cantidad, precioUnitario, precioGlobal;
        cantidad = Float.parseFloat(fieldCantidad.getText());
        precioUnitario = Float.parseFloat(fieldPrecioUnitario.getText());
        precioGlobal = cantidad*precioUnitario;
        fieldPrecioGlobal.setText(""+precioGlobal);
    }//GEN-LAST:event_fieldPrecioGlobalFocusGained

    private void fieldPrecioGlobalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fieldPrecioGlobalActionPerformed

    }//GEN-LAST:event_fieldPrecioGlobalActionPerformed

    private void botonGuardarInsumoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarInsumoActionPerformed
        String nombreInsumo = fieldNombreInsumo.getText();
        String unidadInsumo = fieldUnidad.getText();
        String cantidadInsumo = fieldCantidad.getText();
        String precioUnitario = fieldPrecioUnitario.getText();
        String precioGlobal = fieldPrecioGlobal.getText();
        modeloInsumo.addRow(new Object []{nombreInsumo,unidadInsumo,cantidadInsumo,precioUnitario,precioGlobal});
        fieldNombreInsumo.setText("");
        fieldUnidad.setText("");
        fieldCantidad.setText("");
        fieldPrecioUnitario.setText("");
        fieldPrecioGlobal.setText("");
    }//GEN-LAST:event_botonGuardarInsumoActionPerformed

    private void botonEliminarInsumoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarInsumoActionPerformed
        modeloInsumo.removeRow(tableInsumo.getSelectedRow());
        fieldNombreInsumo.setText("");
        fieldUnidad.setText("");
        fieldCantidad.setText("");
        fieldPrecioUnitario.setText("");
        fieldPrecioGlobal.setText("");
    }//GEN-LAST:event_botonEliminarInsumoActionPerformed

    private void jScrollPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jScrollPane1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jScrollPane1MouseClicked

    private void tableInsumoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableInsumoMouseClicked
        int id = tableInsumo.getSelectedRow();
        fieldNombreInsumo.setText(tableInsumo.getValueAt(id, 0).toString());
        fieldUnidad.setText(tableInsumo.getValueAt(id, 1).toString());
        fieldCantidad.setText(tableInsumo.getValueAt(id, 2).toString());
        fieldPrecioUnitario.setText(tableInsumo.getValueAt(id, 3).toString());
        fieldPrecioGlobal.setText(tableInsumo.getValueAt(id, 4).toString());
    }//GEN-LAST:event_tableInsumoMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonEliminarInsumo;
    private javax.swing.JButton botonGuardarInsumo;
    private javax.swing.JTextField fieldCantidad;
    private javax.swing.JTextField fieldNombreInsumo;
    private javax.swing.JTextField fieldPrecioGlobal;
    private javax.swing.JTextField fieldPrecioUnitario;
    private javax.swing.JTextField fieldUnidad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableInsumo;
    // End of variables declaration//GEN-END:variables
}
