
package GUI;

import javax.swing.table.DefaultTableModel;


public class Proyecto extends javax.swing.JInternalFrame {
    public DefaultTableModel modeloProyecto = new DefaultTableModel();   

    public Proyecto() {
        initComponents();
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        botonGuardarProyecto = new javax.swing.JButton();
        botonEliminarProyecto = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        fieldNombreProyecto = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        fieldCostoProyecto = new javax.swing.JTextField();
        fieldPropietarioProyecto = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        fieldUbicacionProyecto = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableProyecto = new javax.swing.JTable();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Proyecto"));
        setClosable(true);
        setMaximizable(true);

        botonGuardarProyecto.setText("Guardar");
        botonGuardarProyecto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarProyectoActionPerformed(evt);
            }
        });

        botonEliminarProyecto.setText("Eliminar");
        botonEliminarProyecto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarProyectoActionPerformed(evt);
            }
        });

        jLabel3.setText("Propietario:");

        jLabel2.setText("Nombre:");

        jLabel4.setText("Ubicación:");

        jLabel5.setText("Costo (Q):");

        /*
        tableProyecto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        */
        initTableProyecto();
        tableProyecto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableProyectoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableProyecto);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(fieldPropietarioProyecto))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(fieldNombreProyecto, javax.swing.GroupLayout.PREFERRED_SIZE, 447, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(fieldCostoProyecto, javax.swing.GroupLayout.PREFERRED_SIZE, 444, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(fieldUbicacionProyecto, javax.swing.GroupLayout.PREFERRED_SIZE, 446, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonGuardarProyecto, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(botonEliminarProyecto, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 796, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(fieldNombreProyecto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonGuardarProyecto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(fieldPropietarioProyecto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonEliminarProyecto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(fieldUbicacionProyecto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldCostoProyecto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tableProyectoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableProyectoMouseClicked
        int id = tableProyecto.getSelectedRow();
        fieldNombreProyecto.setText(tableProyecto.getValueAt(id, 0).toString());
        fieldPropietarioProyecto.setText(tableProyecto.getValueAt(id, 1).toString());
        fieldUbicacionProyecto.setText(tableProyecto.getValueAt(id, 2).toString());
        fieldCostoProyecto.setText(tableProyecto.getValueAt(id, 3).toString());
    }//GEN-LAST:event_tableProyectoMouseClicked

    private void botonEliminarProyectoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarProyectoActionPerformed
        modeloProyecto.removeRow(tableProyecto.getSelectedRow());
        fieldNombreProyecto.setText("");
        fieldPropietarioProyecto.setText("");
        fieldUbicacionProyecto.setText("");
        fieldCostoProyecto.setText("");        
    }//GEN-LAST:event_botonEliminarProyectoActionPerformed

    private void botonGuardarProyectoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarProyectoActionPerformed
        String nombreProyecto = fieldNombreProyecto.getText();
        String propietarioProyecto = fieldPropietarioProyecto.getText();
        String ubicacionProyecto = fieldUbicacionProyecto.getText();
        String costoProyecto = fieldCostoProyecto.getText();
        modeloProyecto.addRow(new Object[]{nombreProyecto,propietarioProyecto,ubicacionProyecto,costoProyecto});;
        fieldNombreProyecto.setText("");
        fieldPropietarioProyecto.setText("");
        fieldUbicacionProyecto.setText("");
        fieldCostoProyecto.setText("");
    }//GEN-LAST:event_botonGuardarProyectoActionPerformed
    private void initTableProyecto(){
        modeloProyecto.addColumn("Nombre");
        modeloProyecto.addColumn("Propietario");
        modeloProyecto.addColumn("Ubicación");
        modeloProyecto.addColumn("Costo");
        tableProyecto.setModel(modeloProyecto);
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonEliminarProyecto;
    private javax.swing.JButton botonGuardarProyecto;
    private javax.swing.JTextField fieldCostoProyecto;
    private javax.swing.JTextField fieldNombreProyecto;
    private javax.swing.JTextField fieldPropietarioProyecto;
    private javax.swing.JTextField fieldUbicacionProyecto;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableProyecto;
    // End of variables declaration//GEN-END:variables
}
