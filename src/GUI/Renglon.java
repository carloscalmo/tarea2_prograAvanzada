
package GUI;

import javax.swing.table.DefaultTableModel;


public class Renglon extends javax.swing.JInternalFrame {
    public DefaultTableModel modeloRenglon = new DefaultTableModel();   

    
    private void initTableRenglon(){
        modeloRenglon.addColumn("Nombre");
        modeloRenglon.addColumn("Unidad");
        modeloRenglon.addColumn("Cantidad");
        modeloRenglon.addColumn("Precio Unitario");
        modeloRenglon.addColumn("Precio Total");        
        tableRenglon.setModel(modeloRenglon);
    }
    
    public Renglon() {
        initComponents();
        initTableRenglon();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        fieldNombreRenglon = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        fieldUnidad = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        fieldPrecioUnitario = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        fieldPrecioGlobal = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        fieldCantidad = new javax.swing.JTextField();
        botonGuardarRenglon = new javax.swing.JButton();
        botonEliminarRenglon = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableRenglon = new javax.swing.JTable();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Renglón"));
        setClosable(true);
        setMaximizable(true);

        jLabel1.setText("Nombre:");

        jLabel2.setText("Unidad:");

        jLabel3.setText("Precio Unitario:");

        jLabel4.setText("Precio Global:");

        fieldPrecioGlobal.setEditable(false);
        fieldPrecioGlobal.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                fieldPrecioGlobalFocusGained(evt);
            }
        });
        fieldPrecioGlobal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fieldPrecioGlobalActionPerformed(evt);
            }
        });

        jLabel5.setText("Cantidad:");

        botonGuardarRenglon.setText("Guardar");
        botonGuardarRenglon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarRenglonActionPerformed(evt);
            }
        });

        botonEliminarRenglon.setText("Eliminar");
        botonEliminarRenglon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarRenglonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(fieldNombreRenglon, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(fieldPrecioGlobal, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(fieldPrecioUnitario, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)
                                .addComponent(fieldCantidad, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(fieldUnidad, javax.swing.GroupLayout.Alignment.LEADING)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 257, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botonGuardarRenglon)
                            .addComponent(botonEliminarRenglon))
                        .addGap(69, 69, 69))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldNombreRenglon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldUnidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(botonGuardarRenglon))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldPrecioUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(botonEliminarRenglon))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fieldPrecioGlobal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        tableRenglon.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tableRenglon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableRenglonMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableRenglon);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tableRenglonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableRenglonMouseClicked
        int id = tableRenglon.getSelectedRow();
        fieldNombreRenglon.setText(tableRenglon.getValueAt(id, 0).toString());
        fieldUnidad.setText(tableRenglon.getValueAt(id, 1).toString());
        fieldCantidad.setText(tableRenglon.getValueAt(id, 2).toString());
        fieldPrecioUnitario.setText(tableRenglon.getValueAt(id, 3).toString());
        fieldPrecioGlobal.setText(tableRenglon.getValueAt(id, 4).toString());
    }//GEN-LAST:event_tableRenglonMouseClicked

    private void botonGuardarRenglonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarRenglonActionPerformed
        String nombreRenglon = fieldNombreRenglon.getText();
        String unidadRenglon = fieldUnidad.getText();
        String cantidadRenglon = fieldCantidad.getText();
        String precioUnitario = fieldPrecioUnitario.getText();
        String precioGlobal = fieldPrecioGlobal.getText();
        modeloRenglon.addRow(new Object []{nombreRenglon,unidadRenglon,cantidadRenglon,precioUnitario,precioGlobal});      
        fieldNombreRenglon.setText("");
        fieldUnidad.setText("");
        fieldCantidad.setText("");
        fieldPrecioUnitario.setText("");
        fieldPrecioGlobal.setText("");
    }//GEN-LAST:event_botonGuardarRenglonActionPerformed

    private void fieldPrecioGlobalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fieldPrecioGlobalActionPerformed

       
    }//GEN-LAST:event_fieldPrecioGlobalActionPerformed

    private void fieldPrecioGlobalFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fieldPrecioGlobalFocusGained
       Float cantidad, precioUnitario, precioGlobal;
       cantidad = Float.parseFloat(fieldCantidad.getText());
       precioUnitario = Float.parseFloat(fieldPrecioUnitario.getText());
       precioGlobal = cantidad*precioUnitario;
       fieldPrecioGlobal.setText(""+precioGlobal);
    }//GEN-LAST:event_fieldPrecioGlobalFocusGained

    private void botonEliminarRenglonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarRenglonActionPerformed
        modeloRenglon.removeRow(tableRenglon.getSelectedRow());
        fieldNombreRenglon.setText("");
        fieldUnidad.setText("");
        fieldCantidad.setText("");
        fieldPrecioUnitario.setText("");
        fieldPrecioGlobal.setText("");        
    }//GEN-LAST:event_botonEliminarRenglonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonEliminarRenglon;
    private javax.swing.JButton botonGuardarRenglon;
    private javax.swing.JTextField fieldCantidad;
    private javax.swing.JTextField fieldNombreRenglon;
    private javax.swing.JTextField fieldPrecioGlobal;
    private javax.swing.JTextField fieldPrecioUnitario;
    private javax.swing.JTextField fieldUnidad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableRenglon;
    // End of variables declaration//GEN-END:variables
}
