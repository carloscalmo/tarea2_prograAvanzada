package Principal;

import Clases.Insumo;
import Clases.Propietario;
import Clases.Proyecto;
import Clases.Renglon;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConsoleApp {
    public static void main(String[] args) throws Exception{
        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion;

        //listas
        List<Propietario> propietariosList = new ArrayList<Propietario>();
        List<Proyecto> proyectosList = new ArrayList<Proyecto>();
        List<Renglon> renglonesList = new ArrayList<Renglon>();
        List<Insumo> insumosList = new ArrayList<Insumo>();

        while (!salir){
            System.out.println("Bienvenido.\nIngrese la opción que desea ejecutar:\n\n");
            System.out.println("1. Ingresar un nuevo cliente o propietario.\n" +
                                "2. Ingresar un nuevo proyecto.\n" +
                                "3. Ingresar un nuevo renglón.\n" +
                                "4. Listar proyectos.\n"+
                                "5. Listar renglones.\n"+
                                "6. Listar insumos. \n"+
                                "7. Listar clientes.\n"+
                                "9. Salir");
            opcion = sn.nextInt();
            sn.skip("\n");
            switch(opcion){

                //PROPIETARIO
                case 1:
                    //crear objeto del tipo propietario y llenar sus atributos
                    Propietario propietario = new Propietario();
                    System.out.println("Ingrese los datos del nuevo cliente:\n" +
                    "Nombre:");
                    propietario.setNombre(sn.nextLine());
                    System.out.println("Dirección:");
                    propietario.setDireccion(sn.nextLine());
                    System.out.println("Municipio:");
                    propietario.setMunicipio(sn.nextLine());
                    System.out.println("Departamento:");
                    propietario.setDepartamento(sn.nextLine());
                    System.out.println("Nit:");
                    propietario.setNit(sn.nextInt());
                    sn.skip("\n");
                    System.out.println("Teléfono:");
                    propietario.setTelefono(sn.nextInt());
                    sn.skip("\n");
                    //añadir objeto a la lista
                    propietariosList.add(propietario);
                break;

                //PROYECTO
                case 2:
                    //crear objeto de tipo proyecto y llenar sus atributos
                    Proyecto proyecto = new Proyecto();
                    System.out.println("Ingrese los datos del nuevo proyecto:\n" +
                    "Nombre:");
                    proyecto.setNombre(sn.nextLine());
                    System.out.println("Propietario:");
                    proyecto.setPropietario(sn.nextLine());
                    System.out.println("Ubicación:");
                    proyecto.setUbicacion(sn.nextLine());
                    System.out.println("Costo total del proyecto:");

                    //añadir objeto a la lista
                    proyectosList.add(proyecto);
                break;

                //RENGLÓN
                case 3:
                    Renglon renglon = new Renglon();
                    System.out.println("Ingrese los datos del nuevo renglón de trabajo:\n" +
                    "Nombre:");
                    renglon.setNombre(sn.nextLine());
                    System.out.println("Unidad:");
                    renglon.setUnidad(sn.next());

                    //añadir insumos a la lista del renglón
                    boolean bandera = true;
                    while(bandera){
                        //llenar datos del insumo
                        int indicador =0;
                        Insumo insumoRenglon = new Insumo();
                        System.out.println("Ingrese los datos de los insumos que componen el renglón de trabajo:\n" +
                        "Descripción:");
                        insumoRenglon.setDescripcion(sn.next());
                        System.out.println("Unidad:");
                        insumoRenglon.setUnidad(sn.next());
                        System.out.println("Cantidad:");
                        insumoRenglon.setCantidad(sn.nextFloat());
                        System.out.println("Precio unitario:");
                        insumoRenglon.setPrecioUnitario(sn.nextFloat());
                        Float total = insumoRenglon.calculatePrecioTotal();
                        System.out.println("Precio total: "+total);

                        //agregar insumo a la lista
                        insumosList.add(insumoRenglon);

                        //agregar uno más
                        System.out.println("¿Desea agregar un insumo más a este renglón? " +
                        "\n 1. Si \n 2. No");
                        indicador = sn.nextInt();
                        switch(indicador){
                            case 1:
                                bandera = true;
                            break;
                            case 2:
                                bandera = false;
                            break;
                        }
                    }
                    //calcular el precio unitario del renglón
                    float totalRenglon=0;
                    for (int i = 0; i <= insumosList.size()-1; i++){
                        totalRenglon = insumosList.get(i).calculatePrecioTotal()+totalRenglon;
                    }
                    renglon.setPrecioUnitario(totalRenglon);
                    System.out.println("Precio unitario del renglón: "+totalRenglon);

                    //cantidad del renglón y precio global
                    System.out.println("Cantidad de unidades del renglón: ");
                    renglon.setCantidad(sn.nextFloat());

                    System.out.println("Precio global: "+renglon.setPrecioGlobal());

                    //guardar el objeto renglón en la lista
                    renglonesList.add(renglon);
                break;

                //LISTAR PROYECTOS
                case 4:
                    for(int c = 0; c<=(proyectosList.size()-1); c++){
                        System.out.println("Nombre: "+proyectosList.get(c).getNombre()+"\nPropietario: "+proyectosList.get(c).getPropietario()+"\nUbicación: "+proyectosList.get(c).getUbicacion()+"\nCosto: "+proyectosList.get(c).getCosto()+"\n\n");
                    }
                break;

                //LISTAR RENGLONES
                case 5:
                    for(int c = 0; c<=(renglonesList.size()-1); c++){
                        System.out.println("Nombre: "+renglonesList.get(c).getNombre()+"\nUnidad: "+renglonesList.get(c).getUnidad()+"\nCantidad: "+renglonesList.get(c).getCantidad()+"\nPrecio unitario: "+renglonesList.get(c).getPrecioUnitario()+"\nPrecio Global: "+renglonesList.get(c).getPrecioGlobal()+"\n\n");
                    }
                break;

                //LISTAR INSUMOS
                case 6:
                    for(int c=0; c<=insumosList.size()-1; c++){
                        System.out.println("Descripción: "+insumosList.get(c).getDescripcion()+"\nUnidad: "+insumosList.get(c).getUnidad()+"\nCantidad: "+insumosList.get(c).getCantidad()+"\nPrecio unitario: "+insumosList.get(c).getPrecioUnitario()+"\nPrecio Global: "+insumosList.get(c).calculatePrecioTotal()+"\n\n");
                    }
                break;

                case 9:
                    System.exit(0);
                break;
            }
        }

    }
}
